<?php
declare(strict_types=1);
namespace Custom\FirstTemplate\Controller\Page;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
class View extends Action
{
 public function execute()
 {
 /** @var Json $jsonResult */
 return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
}
}
 ?>